# Docker Images for lemmy on arm64

This pipeline uses the official gitlab docker runners to 
Cross Compile [lemmy](https://github.com/LemmyNet/lemmy) and [lemmy-ui](https://github.com/LemmyNet/lemmy-ui) release versions.

The image is pushed to docker hub as [ravermeister/lemmy](https://hub.docker.com/r/ravermeister/lemmy) and 
[ravermeister/lemmy-ui](https://hub.docker.com/r/ravermeister/lemmy-ui)
